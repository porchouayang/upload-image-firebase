const router = require('express').Router();

const uploads = require('../middlewares/uploads');
const cntrl = require('../controllers/upload.controller');
const route = (app) => {
    router.post('/', uploads.single('image'), cntrl.uploadImage);

    return app.use('/uploads', router);
}

module.exports = route;