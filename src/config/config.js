var admin = require("firebase-admin");

var serviceAccount = require("../../serviceAccount.json");

const { getStorage } = require('firebase-admin/storage');
const { getDatabase } = require('firebase-admin/database');


admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://upload-images-e2b97-default-rtdb.asia-southeast1.firebasedatabase.app",
    storageBucket: 'gs://upload-images-e2b97.appspot.com/'
});

const db = getDatabase();
const storage = getStorage();
const bucket = storage.bucket();

module.exports = {
    db,
    storage,
    bucket
}