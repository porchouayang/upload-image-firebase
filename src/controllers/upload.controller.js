const {
    bucket
} = require('../config/config');
const fs = require('fs');
const path = require('path');


module.exports = {
    uploadImage: async (req, res) => {
        const {
            file
        } = req;
        const {
            filename
        } = basedir = path.join(__dirname, '../uploads/' + file.filename);
        await bucket.upload(basedir, {
            destination: filename
        }, (err, file) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    message: 'Error uploading file'
                });
            }
            return res.status(200).json({
                message: 'File uploaded successfully',
                imageId: file.metadata.id,
                imageUrl: file.metadata.mediaLink
            });
        });
    },


}